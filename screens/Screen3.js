import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Linking } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#242423',
    flex: 1,
    justifyContent: 'space-around',
    flexDirection: 'column',
    alignItems: 'center'
  },
  dd: {
    color: '#CFDBD5',
    textAlign: 'center',
    fontSize: 40,
    marginTop: 30
  },
  text: {
    color: '#CFDBD5',
    textAlign: 'center',
    fontSize: 20,
  },
  link: {
    color: '#F5CB5C',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 20
  },
  copy: {
    color: '#F5CB5C',
    textAlign: 'center',
    fontSize: 20,
    marginTop: 50
  },
  image: {
    width: 100,
    height: 100,
    tintColor: '#CFDBD5'
  }
});

export default class Screen3 extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>App created by</Text>
        <Text style={styles.dd}>Dominik Dygowski</Text>
        <Text style={styles.link} onPress={() => Linking.openURL('http://dygowski.net')}>dygowski.net</Text>
        <Text style={styles.text}>using React-Native</Text>
        <Image source={require('../img/react.png')} style={styles.image} />
        <Text style={styles.copy}>&copy; Dygowski</Text>
      </View>
    );
  }
}
