import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Alert } from 'react-native';

export default class Screen2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      yourSide: '',
      compSide: '',
      message: 'choose "X" or "O"',
      pickSide: true,
      makeMove: true,
      wonBlocked: false,
      hardMove: 0,
      compId: '',
      turn: 0,
      position: 0,
      board: [1, 2, 3, 4, 5, 6, 7, 8, 9],
      yourPos: [],
      compPos: [],
      missingPos: [3, 2],
      winCombination: [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
        [1, 4, 7],
        [2, 5, 8],
        [3, 6, 9],
        [1, 5, 9],
        [3, 5, 7]
      ],
      sign1: '',
      sign2: '',
      sign3: '',
      sign4: '',
      sign5: '',
      sign6: '',
      sign7: '',
      sign8: '',
      sign9: ''
    };
    this.pickSide = this.pickSide.bind(this);
    this.gameReset = this.gameReset.bind(this);
    this.checkWin = this.checkWin.bind(this);
    this.endGameCheck = this.endGameCheck.bind(this);
    this.placeSign = this.placeSign.bind(this);
    this.compPutSign = this.compPutSign.bind(this);
    this.tryToWin = this.tryToWin.bind(this);
    this.tryToBlock = this.tryToBlock.bind(this);
    this.tryToFork = this.tryToFork.bind(this);
    this.blockTheFork = this.blockTheFork.bind(this);
    this.playInCorner = this.playInCorner.bind(this);
    this.computerMoveHard = this.computerMoveHard.bind(this);
  }

  async placeSign(e, loc) {
    if ((!this.state.pickSide && this.state.makeMove) && !this.state.compPos.includes(loc)) {
      let turn = this.state.turn;
      turn++;
      switch (loc) {
        case 1:
          let sign1 = this.state.sign1;
          sign1 = this.state.yourSide;
          await this.setState({ sign1 });
          break;
        case 2:
          let sign2 = this.state.sign2;
          sign2 = this.state.yourSide;
          await this.setState({ sign2 });
          break;
        case 3:
          let sign3 = this.state.sign3;
          sign3 = this.state.yourSide;
          await this.setState({ sign3 });
          break;
        case 4:
          let sign4 = this.state.sign4;
          sign4 = this.state.yourSide;
          await this.setState({ sign4 });
          break;
        case 5:
          let sign5 = this.state.sign5;
          sign5 = this.state.yourSide;
          await this.setState({ sign5 });
          break;
        case 6:
          let sign6 = this.state.sign6;
          sign6 = this.state.yourSide;
          await this.setState({ sign6 });
          break;
        case 7:
          let sign7 = this.state.sign7;
          sign7 = this.state.yourSide;
          await this.setState({ sign7 });
          break;
        case 8:
          let sign8 = this.state.sign8;
          sign8 = this.state.yourSide;
          await this.setState({ sign8 });
          break;
        case 9:
          let sign9 = this.state.sign9;
          sign9 = this.state.yourSide;
          await this.setState({ sign9 });
          break;
        default:
          break;
      }
      const yourPos = this.state.yourPos;
      const board = this.state.board;
      let wonBlocked = this.state.wonBlocked;
      wonBlocked = false;
      yourPos.push(loc);
      board.splice(board.indexOf(loc), 1);
      await this.setState({ turn, yourPos, board, wonBlocked });
      this.endGameCheck(yourPos);
    }
  }

  async compPutSign(compId) {
    switch (compId) {
      case 'sign1':
        let sign1 = this.state.sign1;
        sign1 = this.state.compSide;
        await this.setState({ sign1 });
        break;
      case 'sign2':
        let sign2 = this.state.sign2;
        sign2 = this.state.compSide;
        await this.setState({ sign2 });
        break;
      case 'sign3':
        let sign3 = this.state.sign3;
        sign3 = this.state.compSide;
        await this.setState({ sign3 });
        break;
      case 'sign4':
        let sign4 = this.state.sign4;
        sign4 = this.state.compSide;
        await this.setState({ sign4 });
        break;
      case 'sign5':
        let sign5 = this.state.sign5;
        sign5 = this.state.compSide;
        await this.setState({ sign5 });
        break;
      case 'sign6':
        let sign6 = this.state.sign6;
        sign6 = this.state.compSide;
        await this.setState({ sign6 });
        break;
      case 'sign7':
        let sign7 = this.state.sign7;
        sign7 = this.state.compSide;
        await this.setState({ sign7 });
        break;
      case 'sign8':
        let sign8 = this.state.sign8;
        sign8 = this.state.compSide;
        await this.setState({ sign8 });
        break;
      case 'sign9':
        let sign9 = this.state.sign9;
        sign9 = this.state.compSide;
        await this.setState({ sign9 });
        break;
      default:
        break;
    }
  }

  async gameReset() {
    let { yourSide, compSide, pickSide, makeMove, wonBlocked, turn, position, board, yourPos, compPos, message, sign1, sign2, sign3, sign4, sign5, sign6, sign7, sign8, sign9 } = this.state;
    yourSide = '';
    compSide = '';
    pickSide = true;
    makeMove = true;
    wonBlocked = false;
    turn = 0;
    position = 0;
    board = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    yourPos = [];
    compPos = [];
    message = 'choose "X" or "O"';
    sign1 = '';
    sign2 = '';
    sign3 = '';
    sign4 = '';
    sign5 = '';
    sign6 = '';
    sign7 = '';
    sign8 = '';
    sign9 = '';
    await this.setState({ yourSide, compSide, pickSide, makeMove, wonBlocked, turn, position, board, yourPos, compPos, message, sign1, sign2, sign3, sign4, sign5, sign6, sign7, sign8, sign9 });
  }

  async pickSide(e, p) {
    if (this.state.pickSide) {
      let yourSide = this.state.yourSide;
      yourSide = p;
      await this.setState({ yourSide });
      if (this.state.yourSide === 'X') {
        let message = this.state.message;
        let compSide = this.state.compSide;
        message = 'your turn';
        compSide = 'O';
        await this.setState({ message, compSide });
      } else {
        let compSide = this.state.compSide;
        let message = this.state.message;
        message = 'computer\'s turn';
        compSide = 'X';
        await this.setState({ message, compSide });
        setTimeout(() => {
          this.computerMoveHard();
        }, (1000));
      }
      let pickSide = this.state.pickSide;
      pickSide = false;
      await this.setState({ pickSide });
    } else {
      Alert.alert('can\'t change sides during game!');
    }
  }


  checkWin(pos) {
    for (let x = 0; x < 8; x++) {
      if (pos.includes(this.state.winCombination[x][0]) && pos.includes(this.state.winCombination[x][1]) && pos.includes(this.state.winCombination[x][2])) {
        return true;
      }
    }
  }

  async endGameCheck(pos) {
    if (this.checkWin(pos)) {
      if (pos === this.state.compPos) {
        let message = this.state.message;
        message = 'you lost!';
        await this.setState({ message });
      } else {
        let message = this.state.message;
        message = 'you won!';
        await this.setState({ message });
      }
      setTimeout(() => {
        this.gameReset();
      }, (2000));
    } else if (this.state.turn === 9) {
      let message = this.state.message;
      message = 'it\'s a draw!';
      await this.setState({ message });
      setTimeout(() => {
        this.gameReset();
      }, (2000));
    } else {
      if (pos === this.state.compPos) {
        let message = this.state.message;
        message = 'your turn';
        await this.setState({ message });
        let makeMove = this.state.makeMove;
        makeMove = true;
        await this.setState({ makeMove });
      } else {
        let message = this.state.message;
        message = 'computer\'s turn';
        await this.setState({ message });
        let makeMove = this.state.makeMove;
        makeMove = false;
        await this.setState({ makeMove });
        setTimeout(() => {
          this.computerMoveHard();
        }, (1000));
      }
    }
  }

  async tryToWin() {    
    for (let y = 0; y < this.state.winCombination.length; y++) {
      const missingPos = Array.from(this.state.winCombination[y]);
      await this.setState({ missingPos });
      for (let z = 0; z < this.state.compPos.length; z++) {
        if (this.state.winCombination[y].includes(this.state.compPos[z])) {
          missingPos.splice(missingPos.indexOf(this.state.compPos[z]), 1);  
        }
      }
      if (missingPos.length === 1) {
        if (!this.state.yourPos.includes(missingPos[0])) {
          let compId = this.state.compId;
          compId = `sign${missingPos[0]}`;
          const compPos = this.state.compPos;
          compPos.push(missingPos[0]);
          const board = this.state.board;
          board.splice(board.indexOf(missingPos[0]), 1);
          await this.setState({ compId, compPos, board });
          this.compPutSign(compId);
          let wonBlocked = this.state.wonBlocked;
          wonBlocked = true;
          await this.setState({ wonBlocked });
          break;
        }
      }
    }  
  }

  async tryToBlock() {
    for (let y = 0; y < this.state.winCombination.length; y++) {
      const missingPos = Array.from(this.state.winCombination[y]);
      await this.setState({ missingPos });
      for (let z = 0; z < this.state.yourPos.length; z++) {
        if (this.state.winCombination[y].includes(this.state.yourPos[z])) {
          missingPos.splice(missingPos.indexOf(this.state.yourPos[z]), 1);
        }
      }
      if (missingPos.length === 1) {
        if (!this.state.compPos.includes(missingPos[0])) {
          let compId = this.state.compId;
          compId = `sign${missingPos[0]}`;
          const compPos = this.state.compPos;
          compPos.push(missingPos[0]);
          const board = this.state.board;
          board.splice(board.indexOf(missingPos[0]), 1);
          await this.setState({ compId, compPos, board });
          this.compPutSign(compId);
          let wonBlocked = this.state.wonBlocked;
          wonBlocked = true;
          await this.setState({ wonBlocked });
          break;
        }
      }
    }
  }

  async tryToFork() {
    if (this.state.compPos.includes(1) && this.state.compPos.includes(9)) {
      if (!this.state.yourPos.includes(3)) {
        const compPos = this.state.compPos;
        const board = this.state.board;
        compPos.push(3);
        board.splice(board.indexOf(3, 1));
        await this.setState({ compPos, board });
        this.compPutSign('sign3');
        let wonBlocked = this.state.wonBlocked;
        wonBlocked = true;
        await this.setState({ wonBlocked });
      } else if (!this.state.yourPos.includes(7)) {
        const compPos = this.state.compPos;
        const board = this.state.board;
        compPos.push(7);
        board.splice(board.indexOf(7, 1));
        await this.setState({ compPos, board });
        this.compPutSign('sign7');
        let wonBlocked = this.state.wonBlocked;
        wonBlocked = true;
        await this.setState({ wonBlocked });
      }
    } else if (this.state.compPos.includes(3) && this.state.compPos.includes(7)) {
      if (!this.state.yourPos.includes(1)) {
        const compPos = this.state.compPos;
        const board = this.state.board;
        compPos.push(1);
        board.splice(board.indexOf(1, 1));
        await this.setState({ compPos, board });
        this.compPutSign('sign1');
        let wonBlocked = this.state.wonBlocked;
        wonBlocked = true;
        await this.setState({ wonBlocked });
      } else if (!this.state.yourPos.includes(9)) {
        const compPos = this.state.compPos;
        const board = this.state.board;
        compPos.push(9);
        board.splice(board.indexOf(9, 1));
        await this.setState({ compPos, board });
        this.compPutSign('sign9');
        let wonBlocked = this.state.wonBlocked;
        wonBlocked = true;
        await this.setState({ wonBlocked });
      }
    }
  }

  async blockTheFork() {
    if ((this.state.yourPos.includes(1) && this.state.yourPos.includes(9)) || (this.state.yourPos.includes(3) && this.state.yourPos.includes(7))) {
      const sidePos = [2, 4, 6, 8];
      for (let i = 0; i < 4; i++) {
        if (!this.state.yourPos.includes(sidePos[i]) && !this.state.compPos.includes(sidePos[i])) {
          let compId = this.state.compId;
          compId = `sign${sidePos[i]}`;
          const compPos = this.state.compPos;
          const board = this.state.board;
          compPos.push(sidePos[i]);
          board.splice(board.indexOf(sidePos[i], 1));
          await this.setState({ compId, compPos, board });
          this.compPutSign(compId);
          let wonBlocked = this.state.wonBlocked;
          wonBlocked = true;
          await this.setState({ wonBlocked });
          break;
        }
      }
    }
  }

  async playInCorner() {
    for (let j = 0; j < 4; j++) {
      for (let k = 0; k < 4; k++) {
        const cornerPos = [1, 3, 7, 9];
        if (this.state.yourPos.includes(cornerPos[j]) && this.state.yourPos.includes(5)) {
          if (!this.state.yourPos.includes(cornerPos[k]) && !this.state.compPos.includes(cornerPos[k])) {
            let compId = this.state.compId;
            compId = `sign${cornerPos[k]}`;
            const compPos = this.state.compPos;
            const board = this.state.board;
            compPos.push(cornerPos[k]);
            board.splice(board.indexOf(cornerPos[k], 1));
            await this.setState({ compId, compPos, board });
            this.compPutSign(compId);
            let wonBlocked = this.state.wonBlocked;
            wonBlocked = true;
            await this.setState({ wonBlocked });
            break;
          }
        }
      }
    }
  }

  async computerMoveHard() {
    let turn = this.state.turn;
    turn += 1;
    await this.setState({ turn });
    if (turn === 1) {
      console.log('turn1 move')
      let hardMove = this.state.hardMove;
      hardMove = [1, 3, 7, 9][Math.floor(Math.random() * 4)];
      const compPos = this.state.compPos;
      const board = this.state.board;
      compPos.push(hardMove);
      board.splice(board.indexOf(hardMove), 1);
      let compId = this.state.compId;
      compId = `sign${hardMove}`;
      await this.setState({ hardMove, compId, compPos, board });
      this.compPutSign(compId);
      let message = this.state.message;
      message = 'your turn';
      await this.setState({ message });
    } else if (turn === 2) {
      console.log('turn2 move')
      if (this.state.yourPos.includes(5)) {
        let hardMove = this.state.hardMove;
        hardMove = [1, 3, 7, 9][Math.floor(Math.random() * 4)];
        const compPos = this.state.compPos;
        const board = this.state.board;
        compPos.push(hardMove);
        board.splice(board.indexOf(hardMove), 1);
        let compId = this.state.compId;
        compId = `sign${hardMove}`;
        await this.setState({ hardMove, compId, compPos, board });
        this.compPutSign(compId);
      } else {
        const compPos = this.state.compPos;
        const board = this.state.board;
        compPos.push(5);
        board.splice(board.indexOf(5), 1);
        await this.setState({ compPos, board });
        this.compPutSign('sign5');
      }
      let message = this.state.message;
      message = 'your turn';
      await this.setState({ message });
    } else if (turn === 3) {
      console.log('turn3 move');
      if (!this.state.yourPos.includes(5)) {
        const compPos = this.state.compPos;
        const board = this.state.board;
        compPos.push(5);
        board.splice(board.indexOf(5), 1);
        await this.setState({ compPos, board });
        this.compPutSign('sign5');
      } else {
        const lastCompMove = this.state.hardMove;
        if (lastCompMove === 1) {
          const compPos = this.state.compPos;
          const board = this.state.board;
          compPos.push(9);
          board.splice(board.indexOf(9), 1);
          await this.setState({ compPos, board });
          this.compPutSign('sign9');
        } else if (lastCompMove === 3) {
          const compPos = this.state.compPos;
          const board = this.state.board;
          compPos.push(7);
          board.splice(board.indexOf(7), 1);
          await this.setState({ compPos, board });
          this.compPutSign('sign7');
        } else if (lastCompMove === 7) {
          const compPos = this.state.compPos;
          const board = this.state.board;
          compPos.push(3);
          board.splice(board.indexOf(3), 1);
          await this.setState({ compPos, board });
          this.compPutSign('sign3');
        } else {
          const compPos = this.state.compPos;
          const board = this.state.board;
          compPos.push(1);
          board.splice(board.indexOf(1), 1);
          await this.setState({ compPos, board });
          this.compPutSign('sign1');
        }
      }
      let message = this.state.message;
      message = 'your turn';
      await this.setState({ message });
    } else {
      console.log('trying to win');
      console.log(`wonBlocked: ${this.state.wonBlocked}`);
      await this.tryToWin();
      if (this.state.wonBlocked) {
        await this.endGameCheck(this.state.compPos);
      } else {
        console.log('trying to block');
        console.log(`wonBlocked: ${this.state.wonBlocked}`);
        await this.tryToBlock();
        if (this.state.wonBlocked) {
          await this.endGameCheck(this.state.compPos);
        } else {
          console.log('trying to fork');
          console.log(`wonBlocked: ${this.state.wonBlocked}`);
          await this.tryToFork();
          if (this.state.wonBlocked) {
            await this.endGameCheck(this.state.compPos);
          } else {
            console.log('blocking fork');
            console.log(`wonBlocked: ${this.state.wonBlocked}`);
            await this.blockTheFork();
            if (this.state.wonBlocked) {
              await this.endGameCheck(this.state.compPos);
            } else {
              console.log('playing in corner');
              console.log(`wonBlocked: ${this.state.wonBlocked}`);
              await this.playInCorner();
              if (this.state.wonBlocked) {
                await this.endGameCheck(this.state.compPos);
              } else {
                console.log('easy move')
                console.log(`wonBlocked: ${this.state.wonBlocked}`)
                let easyMove = this.state.easyMove;
                const compPos = this.state.compPos;
                const board = this.state.board;
                easyMove = board[Math.floor(Math.random() * board.length)];
                compPos.push(easyMove);
                board.splice(board.indexOf(easyMove), 1);
                let compId = this.state.compId;
                compId = `sign${easyMove}`;
                await this.setState({ compId, compPos, board, easyMove });
                this.compPutSign(compId);
                this.endGameCheck(this.state.compPos);
              }
            }
          }
        }
      }
    }
    let makeMove = this.state.makeMove;
    makeMove = true;
    await this.setState({ makeMove });
  }


  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.ttt}>Tic Tac Toe</Text>
        <View style={styles.choose}>
          <TouchableOpacity onPress={(e) => this.pickSide(e, 'X')}>
            <Text style={styles.x}>X</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={(e) => this.pickSide(e, 'O')}>
            <Text style={styles.o}>O</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.text}>{this.state.message}</Text>
        <View style={styles.gameboard}>
          <View style={styles.row}>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 1)}>
              <View style={styles.sq1}>
                <Text style={styles.sign}>{this.state.sign1}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 2)}>
              <View style={styles.sq2}>
                <Text style={styles.sign}>{this.state.sign2}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 3)}>
              <View style={styles.sq3}>
                <Text style={styles.sign}>{this.state.sign3}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.row}>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 4)}>
              <View style={styles.sq4}>
                <Text style={styles.sign}>{this.state.sign4}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 5)}>
              <View style={styles.sq5}>
                <Text style={styles.sign}>{this.state.sign5}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 6)}>
              <View style={styles.sq6}>
                <Text style={styles.sign}>{this.state.sign6}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.row}>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 7)}>
              <View style={styles.sq7}>
                <Text style={styles.sign}>{this.state.sign7}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 8)}>
              <View style={styles.sq8}>
                <Text style={styles.sign}>{this.state.sign8}</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={(e) => this.placeSign(e, 9)}>
              <View style={styles.sq9}>
                <Text style={styles.sign}>{this.state.sign9}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#242423',
    flex: 1,
    alignItems: 'center'
  },
  ttt: {
    color: '#CFDBD5',
    textAlign: 'center',
    fontSize: 40,
    marginTop: 30
  },
  choose: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  x: {
    color: 'green',
    fontSize: 50,
    marginRight: 50
  },
  o: {
    color: 'red',
    fontSize: 50,
    marginLeft: 50
  },
  text: {
    color: '#CFDBD5',
    textAlign: 'center',
    fontSize: 20
  },
  gameboard: {
    flex: 1,
    marginTop: 20
  },
  row: {
    flexDirection: 'row'
  },
  sq1: {
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq2: {
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderLeftWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq3: {
    borderLeftWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq4: {
    borderTopWidth: 1,
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq5: {
    borderWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq6: {
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq7: {
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq8: {
    borderRightWidth: 1,
    borderTopWidth: 1,
    borderLeftWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sq9: {
    borderLeftWidth: 1,
    borderTopWidth: 1,
    borderColor: '#E8EDDF',
    height: 100,
    width: 100
  },
  sign: {
    color: '#F5CB5C',
    fontSize: 80,
    textAlign: 'center'
  }
});
