import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#242423',
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  welcome: {
    color: '#CFDBD5',
    textAlign: 'center',
    fontSize: 30
  },
  ttt: {
    color: '#CFDBD5',
    textAlign: 'center',
    fontSize: 40,
    margin: 30
  },
  text: {
    color: '#CFDBD5',
    textAlign: 'center',
    fontSize: 20,
    // flex: 1
  },
  image: {
    width: 100,
    height: 100,
    tintColor: '#CFDBD5'
  }
});

export default class Screen1 extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to</Text>
        <Text style={styles.ttt}>Tic Tac Toe!</Text>
        <Text style={styles.text}>the game where you can't win</Text>
        <Text style={styles.text}>and points don't matter</Text>
        <Image source={require('../img/play.png')} style={styles.image} />
      </View>
    );
  }
}
