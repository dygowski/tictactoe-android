import { Navigation } from 'react-native-navigation';

import Screen1 from './Screen1';
import Screen2 from './Screen2';
import Screen3 from './Screen3';

// register all screens of the app (including internal ones)
export default () => {
  Navigation.registerComponent('Screen1', () => Screen1);
  Navigation.registerComponent('Screen2', () => Screen2);
  Navigation.registerComponent('Screen3', () => Screen3);

  Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'Home',
        screen: 'Screen1',
        icon: require('../img/home.png'),
        selectedIcon: require('../img/home.png'),
        title: 'Tic Tac Toe'
      },
      {
        label: 'PLAY',
        screen: 'Screen2',
        icon: require('../img/play.png'),
        selectedIcon: require('../img/play.png'),
        title: 'Play'
      },
      {
        label: 'About',
        screen: 'Screen3',
        icon: require('../img/about.png'),
        selectedIcon: require('../img/about.png'),
        title: 'About'
      }
    ],
    appStyle: {
      tabBarBackgroundColor: '#333533',
      tabBarButtonColor: '#CFDBD5',
      tabBarSelectedButtonColor: '#F5CB5C',
      navBarTitleTextCentered: true,
      navBarBackgroundColor: '#333533',
      navBarTextColor: '#F5CB5C'
    }
  });
};
